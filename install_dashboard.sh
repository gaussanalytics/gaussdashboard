#!/bin/sh

# TODO: Add try-catch clause
# TODO: Add script to install neccessary depndencies below if not already setup locally
# TODO: change the version as aproriate (currently hard coded)
# TODO: must check that the version of python is 3.5+
# TODO: make this option a command line arguement

## Setting up Node.js ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# NOTE: to setup node.js and npm see:
  # 1. http://stackoverflow.com/questions/9044788/how-do-i-uninstall-nodejs-installed-from-pkg-mac-os-x
  # 2. https://github.com/airbnb/caravel/blob/master/CONTRIBUTING.md
#  ------------
# first if node is already installed, uninstall it:
# sudo rm -rf /usr/local/lib/node /usr/local/lib/node_modules /var/db/receipts/org.nodejs.*
# sudo rm -rf /usr/local/include/node /Users/$USER/.npm
# sudo rm /usr/local/bin/node
# sudo rm /usr/local/share/man/man1/node.1
# sudo rm /usr/local/bin/npm
# sudo rm /usr/local/share/systemtap/tapset/node.stp
# sudo rm /usr/local/lib/dtrace/node.d

# brew uninstall node

# # now install node.js (build it from scratch)
# brew install node --without-npm
# echo prefix=~/.npm-packages >> ~/.npmrc
# curl -L https://www.npmjs.com/install.sh | sh
# echo 'export PATH=$HOME/.npm-packages/bin:$PATH' >>~/.bash_profile
# source ~/.bashrc
#  ------------

## Setting up anaconda and switching to python 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Note: to install anaconda see:
# . https://docs.continuum.io/anaconda/install
# create a python 3.5 env
# conda create --name python3_dev python=3
# # switch to python 3.5 env
# source activate python3_dev


function print_help { echo "Usage $0 [-r] " >&2 ;
exit 1; }

while getopts r OPT; do
  case $OPT in
    r)
      REBUILD_DASHBOARD=true
      ;;
    --help)  #show help
      print_help
      ;;
    \?) #unrecognized option - show help
      print_help
      ;;
  esac
done

# by default dont attempt to rebuild the package
REBUILD_DASHBOARD==${REBUILD_DASHBOARD:-false}

# Dependencies -----------
# node -v
# -> v6.3.1
# npm -v
# -> 3.10.3
# pip --version
# -> pip 8.1.2 (python 3.5)
# python 3.5 env
# --------------------------

# get the dashboard directory (even if accessed from a different dir or a symlink)
# see here: https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-bash-script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DASHBOARD_HOME_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#echo $DASHBOARD_HOME_DIR
CURRENT_DIR=$PWD
#echo $CURRENT_DIR
cd $DASHBOARD_HOME_DIR
#DASHBOARD_HOME_DIR="/Users/oliversaleh/work/Gauss/gauss_repo/dashboard"

cd $DASHBOARD_HOME_DIR
if [ $REBUILD_DASHBOARD == true ]; then
	./build_caravel.sh
fi

caravel_build=caravel-0.10.0.tar.gz
caravel_build_path=$GAUSS_REPO_ROOT/gaussdashboard/dist/$caravel_build
if [ -f "$caravel_build_path" ]
then
	echo "$caravel_build found; Installing..."
else
	echo "$caravel_build not found; You need to build the package. Change directory to the root of the project and run: './build_caravel.sh'"
	exit 1
fi
# ===========================
# open new window and start http server
mkdir dist
cd dist
# start http server on port 9000 bound to local host (in background)
echo "Starting pip server in the background on http://127.0.0.1:9000"
python -m http.server 9000 --bind 127.0.0.1 &
# ===========================
# go back to original window
echo "Attempting to uninstall any previous version of caravel installed..."
pip uninstall caravel

echo "Installing caravel from the package file $caravel_build_path"
pip install  http://127.0.0.1:9000/caravel-0.10.0.tar.gz
echo "Installation complete"
echo "Tearing down pip file server..."
# kill the server started earlier
ps aux | grep "python -m http.server" | awk '{print $2}' | xargs kill -9

# switch away from python 3.5 env
# source deactivate

cd $CURRENT_DIR