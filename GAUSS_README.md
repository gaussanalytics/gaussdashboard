Setting up the dashboard
1. Building the dashboard image (post development) - On Mac-OSX

# get the latest version of the dashboard source code
# set the GAUSS_REPO_ROOT enviromrntal variable to point to the Gauss project root
cd $GAUSS_REPO_ROOT
git clone https://oliver-gauss-io@bitbucket.org/gaussanalytics/gaussdashboard.git
cd gaussdashboard

# Prepare your system for caravel
# see here for more: http://airbnb.io/caravel/installation.html

# Install anaconda on your system
# see https://docs.continuum.io/anaconda/install

# create a python 3.5 env
conda create --name python3_dev python=3
# switch to python 3.5 env
source activate python3_dev

# instal node & npm
# NOTE: to setup node.js and npm see:
  # 1. http://stackoverflow.com/questions/9044788/how-do-i-uninstall-nodejs-installed-from-pkg-mac-os-x
  # 2. https://github.com/airbnb/caravel/blob/master/CONTRIBUTING.md
#  ------------
# first if node is already installed, uninstall it:
sudo rm -rf /usr/local/lib/node /usr/local/lib/node_modules /var/db/receipts/org.nodejs.*
sudo rm -rf /usr/local/include/node /Users/$USER/.npm
sudo rm /usr/local/bin/node
sudo rm /usr/local/share/man/man1/node.1
sudo rm /usr/local/bin/npm
sudo rm /usr/local/share/systemtap/tapset/node.stp
sudo rm /usr/local/lib/dtrace/node.d

brew uninstall node

# now install node.js (build it from scratch)
brew install node --without-npm
echo prefix=~/.npm-packages >> ~/.npmrc
curl -L https://www.npmjs.com/install.sh | sh
echo 'export PATH=$HOME/.npm-packages/bin:$PATH' >>~/.bash_profile
source ~/.bashrc
#  ------------

## Setting up anaconda and switching to python 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Note: to install anaconda see:
# . https://docs.continuum.io/anaconda/install
# create a python 3.5 env
# conda create --name python3_dev python=3
# # switch to python 3.5 env
# source activate python3_dev


# Build the caravel pip image
./build_caravel.sh

2. Installing the caravel dashboard locally and configuring it

# Install the caravel image on the local machine
./install_dashboard.sh


# Create an admin user
fabmanager create-admin --app caravel

# Initialize the database
caravel db upgrade

# Create default roles and permissions
caravel init

# Load some data to play with
caravel load_examples

# Start the web server on port 8088
caravel runserver -p 8088

# To start a development web server, use the -d switch
caravel runserver -d


3. Create a Docker image and spin up a docker container

# install docker on your system
# obtain the caravel tar.gz image (either the one built above in step.1 or a previously built one) and place it in the dist directory (at the root of the project)
cd Docker
./build_docker_image.sh -b -d
# this  should build a new docker image called gauss/gaussdashboard:latest and create a new container based on that image called funkyGaussian
# if these already exist on your system you may want to remove them first

4. Configure the dashboard
TBD


