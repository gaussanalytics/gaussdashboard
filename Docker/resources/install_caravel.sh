cd tmp/gauss_dashboard
# uninstal caravel if already installed
pip3 uninstall caravel
python3 -m http.server 9000 --bind 127.0.0.1 &
pip3 install http://127.0.0.1:9000/caravel-0.10.0.tar.gz
ps aux | grep "python -m http.server" | awk '{print $2}' | xargs kill -9
ls