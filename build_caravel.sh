## build_caravel.sh
#!/bin/sh

# TODO: Add try-catch clause
# TODO: Add script to install neccessary depndencies below if not already setup locally

# Dependencies -----------
# node -v
# -> v6.3.1
# npm -v
# -> 3.10.3
# webpack
# -> 1.13.1 
# pip --version
# -> pip 8.1.2 (python 3.5)
# python 3.5 env
# --------------------------

function backup () {
	             
	BACKUPALL=0
	for entry in "$1"/*
	do
		if [ $BACKUPALL == 1 ]; then
			# backup without asking
			cp "$entry" "$entry".bak
		elif [ $BACKUPALL == 0 ]; then
		  echo "Would you like to make a back-up of "$entry
		  select yn in "Yes" "No" "YesForAll" "NoForAll"; do
				    case $yn in
				        Yes )
						cp "$entry" "$entry".bak
						break
						;;
				        No )
						break
						;;
						YesForAll )
						cp "$entry" "$entry".bak
						BACKUPALL=1
						break
						;;
						NoForAll )
						BACKUPALL=2
						break
						;;
				    esac
				done
		# else dont backup without asking
		fi
		echo "Deleting file"$entry
		rm $entry
	done
}


# get the dashboard directory (even if accessed from a different dir or a symlink)
# see here: https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-bash-script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DASHBOARD_HOME_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
#echo $DASHBOARD_HOME_DIR
CURRENT_DIR=$PWD
#echo $CURRENT_DIR
cd $DASHBOARD_HOME_DIR
#DASHBOARD_HOME_DIR="/Users/oliversaleh/work/Gauss/gauss_repo/dashboard"

#switch to python 3.5 env
# source activate python3_dev
echo "Building caravel package..."
# open new cmd window
cd $DASHBOARD_HOME_DIR
# delete dist directory if it exists
echo "Clearing existing content in the dist directory (including possibly past caravel built packages)"
backup $DASHBOARD_HOME_DIR/dist
# build python package

echo "running <npm install> and <npm run prod>..."
cd caravel/assets/
# TODO: the command below is failing
# i installed node.js, as well as npn
# i also installed webpack: npm i -g webpack
# im getting error: ERROR in Entry module not found: Error: Cannot resolve module 'babel' in /Users/oliversaleh/work/Gauss/gauss_repo/gaussdashboard/caravel/assets
# the resulting build has display issues
npm install
npm run prod
# or: npm run dev
cd ../..
echo "Building the pip package (as a tar.gz package) in the dist directory"
python setup.py sdist
# this will create a dist dir containing a tar.gz package

echo "Caravel has been successfully built and packaged for pip deployment!"

cd $CURRENT_DIR