#============================================================
# Parse input flags, options, and arguments
function print_help { 
echo "Usage: $0 [-b | --build-docker-image ] [ -I | --docker-image-name [<IMAGE_NAME>] ]  [-c | --create-docker-container) ] [-N | --docker-container-name [<CONTAINER_NAME>] ] [-p | --pip-package <PACKAGE_PATH>" >&2
echo "    [-b | --build-docker-image]: builds a docker image; may need to build an actual caravel pip package first if one is not found" >&2
echo "    [-I | --docker-image-name [<IMAGE_NAME>]]: specifies a docker image name; if no image name is specified, it will revert to the default name of <gauss/gaussdashboard>" >&2
echo "    [-c | --create-docker-container) ]: creates a docker container based on a pre-existing (or newly created) docker image" >&2
echo "    [-N | --docker-container-name) [<CONTAINER_NAME>]]: specifies a docker container name; if no container name is specified, it will revert to the default name of <funkyGaussian>" >&2
echo "    [-p | --pip-package <PACKAGE_PATH>]: receives link to the pip package you wish to use; the package must be a valid pip package in tar.gz format" >&2
exit 1; 
}

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--build-docker-image)
    BUILD_DOCKER_IMAGE=true
    shift
    ;;
    -I|--docker-image-name)
    if [ "$2" == ''  -o "$2" == ' ' -o "${2:0:1}" == '-' ] ; then
        : #echo "No image name specified - reverting to default"
    else
        docker_image_name=$2
        shift
    fi
    shift
    ;;
     -c|--create-docker-container)
    CREATE_DOCKER_CONTAINER=true
    shift
    ;;
    -N|--docker-container-name)
    if [ "$2" == ''  -o "$2" == ' ' -o "${2:0:1}" == '-' ] ; then
        : #echo "No container name specified - reverting to default"
    else
        docker_container_name=$2
        shift
    fi
    shift
    ;;
    -p|--pip-package)
    if [ "$2" == ''  -o "$2" == ' ' -o "${2:0:1}" == '-' ] ; then
        echo "No path to a pip package provided - ignoring this option"
        USE_PACKAGE_PATH=false
    else
        USE_PACKAGE_PATH=true
        full_pip_package_path=$2
        shift
    fi
    shift
    ;;
    -h|--help)
    print_help
    shift
    ;;
    -*)
    echo "Unknown option: $1"
    print_help
    exit 1
    ;;
    *)
    echo "Unknown argument provided: $1"
    print_help
    exit 1
    ;;
esac
done


BUILD_DOCKER_IMAGE=${BUILD_DOCKER_IMAGE:-false}
CREATE_DOCKER_CONTAINER=${CREATE_DOCKER_CONTAINER:-false}
docker_container_name=${docker_container_name:-'funkyGaussian'}
docker_image_name=${docker_image_name:-'gauss/gaussdashboard'}
full_pip_package_path=${full_pip_package_path:-$GAUSS_REPO_ROOT/gaussdashboard/dist/caravel-0.10.0.tar.gz}


# Check if an image with the same name exists - if so exit and warn user
EXISTINGIMAGE=$(docker images | grep ${docker_image_name})
if [ $BUILD_DOCKER_IMAGE == true ] && [ -n "$EXISTINGIMAGE" ]; then
    echo 'The Docker image name '$docker_image_name' provided already is in use on the local system; please pick a different name and try again.'
    exit 1
fi
echo 'Docker Image Name: '${docker_image_name}

# Check if container with same name and belonging to same image exists - if so exit and warn user
# Check if an image with the same name exists - if so exit and warn user
EXISTINGCONTAINER=$(docker ps -a | grep ${docker_container_name})
if [ $CREATE_DOCKER_CONTAINER == true ] && [ -n "$EXISTINGCONTAINER" ] && [ -n "$EXISTINGIMAGE" ];then
    echo 'The Docker container name '$docker_container_name' provided already is in use on the local system for image '${docker_container_name}'; please pick a different name and try again.'
    exit 1
fi
echo 'Docker Container Name: '${docker_container_name}

pip_package_name=$(basename "$full_pip_package_path")
pip_package_path=${full_pip_package_path%/*}

if [[ 0 == 1 ]]; then
    echo "CREATE_DOCKER_CONTAINER: $CREATE_DOCKER_CONTAINER"
    echo "BUILD_DOCKER_IMAGE: $BUILD_DOCKER_IMAGE"
    echo "docker_container_name: $docker_container_name"
    echo "docker_image_name: $docker_image_name"
    echo "full_pip_package_path: $full_pip_package_path"
    exit 0
fi
# ============================================================
# get the dashboard directory (even if accessed from a different dir or a symlink)
# see here: https://stackoverflow.com/questions/1371261/get-current-directory-name-without-full-path-in-bash-script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
SCRIPT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
CURRENT_DIR=$PWD

# cd $SCRIPT_DIR
# replace existing caravel file (if any)

if [ $BUILD_DOCKER_IMAGE == true ]; then
	echo "Building a Docker image for the Gauss Dashboard"
	if [ -f "$full_pip_package_path" ]; then
		echo "Pip package found at: $full_pip_package_path"
	else
		echo "No pip package found at path specified: $full_pip_package_path; would you like to try to build it?"
		select yn in "Yes" "No"; do
		    case $yn in
		        Yes)
			cd ..
			./build_caravel.sh
			cd $SCRIPT_DIR
			break
			;;
		        *)
			exit 1
			;;
		    esac
		done
	fi
	yes | cp -rf $full_pip_package_path  "$SCRIPT_DIR/resources/$pip_package_name"
	docker build -t $docker_image_name $SCRIPT_DIR
	echo "Docker image built successfully"
fi
docker images
# TODO: testing below fails - need to fix this; i noticed adding in the --env variables causes problems but 'works' without them...
if [ $CREATE_DOCKER_CONTAINER == true ]; then
	echo "Creating docker container"
	docker run --detach \
		--name $docker_container_name \
	    --env SECRET_KEY="qnTT5+9*45eeW!jodP" \
	    --env SQLALCHEMY_DATABASE_URI="redshift+psycopg2://oliver@iri-dataset.chz8mzfebvxp.us-west-2.redshift.amazonaws.com:5439/iridataset" \
	    --publish 8088:8088 \
	    $docker_image_name:latest

	docker exec -it $docker_container_name caravel-init
	echo "Docker container created and initialized successfully - you can access the gauss dashboard on port 127.0.0.1:8088"
	docker ps
fi

# # note: to delete an image
# # get list of al images:
# docker images
# # find the image you want to delete and delete it (you may need to stop and delete any associated containers)
# docker rmi <IMAGE_ID>
# # note:  to delete a container
# # get a list of all containers
# docker ps -a
# # find the container in the list; if it is running stop it first
# docker stop <CONTAINER_NAME>
# # delete the container
# docker rm <CONTAINER_NAME>

echo "Cleaning up.."
if [ -f "$SCRIPT_DIR/resources/$pip_package_name" ]; then
    rm "$SCRIPT_DIR/resources/$pip_package_name"
fi
cd $CURRENT_DIR
